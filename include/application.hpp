#pragma once
#include "ublox.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include <thread>
#include <csignal>

class Application {
private:
    inline static bool running = false;
    ublox::GPS* gps;

    static void termination_handler(int) {
        spdlog::info("Termination requested");
        running = false;
    }

public:
    Application() : gps(nullptr) {}
    ~Application() {}

    void init_log() {
        auto file_logger = spdlog::basic_logger_mt("gps", "/tmp/gps.log");
        spdlog::set_default_logger(file_logger);
        spdlog::default_logger()->flush_on(spdlog::level::level_enum::info);
    }

    void init_signals() {
        std::signal(SIGTERM, Application::termination_handler);
    }

    int exec() {
        init_log();        
        init_signals();
        spdlog::info("Starting app...");
        try {
            gps = new ublox::GPS();
            running = true;
        } catch (const std::exception& e) {
            spdlog::error(e.what());
            gps = nullptr;
        }
        while (running) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            if ( gps != nullptr) {
                try {
                    gps->process();
                } catch (const ublox::network_error& e) {
                    spdlog::critical(e.what());
                    running = false;
                }
            } else {
                running = false;
            }
        }
        delete gps;
        return 0;
    }
};
