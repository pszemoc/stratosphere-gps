#pragma once
#include <exception>
#include <chrono>
#include <vector>
#include <string>
#include <zmq.hpp>
#include "gpsframe.hpp"

namespace ublox {
    namespace nmea {
        constexpr int time_pos = 1;
        constexpr int lat_pos = 2;
        constexpr int lat_dir_pos = 3;
        constexpr int long_pos = 4;
        constexpr int long_dir_pos = 5;
        constexpr int fix_quality_pos = 6;
        constexpr int satelites_pos = 7;
        constexpr int alt_pos = 9;
    }
    
    class config_error : public std::runtime_error {
    public:
        config_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class network_error : public std::runtime_error {
    public:
        network_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class device_error : public std::runtime_error {
    public:
        device_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class parse_error : public std::runtime_error {
    public:
        parse_error(const char* what_arg) : std::runtime_error(what_arg) {}
    };

    class GPS {
    private:
        static constexpr int                    serial_buffer_length = 4096;

        bool                                    enabled;
        zmq::socket_t*                          socket;
        zmq::context_t                          context;
        int                                     serial;
        std::string                             serial_dev;
        int                                     serial_baud;
        char                                    serial_buffer[serial_buffer_length];
        std::chrono::system_clock::time_point   last_device_transmission;

        std::vector<std::string>                split(std::string str, char delimg);
        std::string                             read_data();
        std::vector<gps_frame_t>                parse_data(std::string data);
        void                                    send_data(std::vector<gps_frame_t>& gps_frames);

    public:
        GPS();
        ~GPS();
        void process();
        bool is_enabled();
        bool is_healthy();
    };
};