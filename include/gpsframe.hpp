#pragma once

struct gps_frame_t {
    int     time;
    double  lat;
    char    lat_dir;
    double  lon;
    char    lon_dir;
    int    fix_quality;
    int    satelites;
    double  altitude;

    gps_frame_t() {}

    gps_frame_t(
        int     time,
        double  lat,
        char    lat_dir,
        double  lon,
        char    lon_dir,
        char    fix_quality,
        char    satelites,
        double  altitude
    )
    : time(time)
    , lat(lat)
    , lat_dir(lat_dir)
    , lon(lon)
    , lon_dir(lon_dir)
    , fix_quality(fix_quality)
    , satelites(satelites)
    , altitude(altitude)
    {}
};