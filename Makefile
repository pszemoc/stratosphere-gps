CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:= -lconfig++ -lzmq
EXECUTABLE	:= main


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*

install:
	sudo cp ./$(BIN)/$(EXECUTABLE) /usr/bin/sp-gps
	sudo mkdir -p /etc/sp
	sudo cp sp.cfg /etc/sp/
	sudo cp sp-gps.service /etc/systemd/system/

uninstall:
	sudo rm /usr/bin/sp-gps /etc/sp/sp.cfg /etc/systemd/system/sp-gps.service
