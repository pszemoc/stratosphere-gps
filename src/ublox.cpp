#include "ublox.hpp"
#include "wiringSerial.h"
#include "spdlog/spdlog.h"
#include <sstream>
#include <libconfig.h++>
#include <algorithm>
#include <unistd.h>
#include <fcntl.h>
#include <exception>

ublox::GPS::GPS() : enabled(false), socket(nullptr) {
    libconfig::Config cfg;
    try {
        cfg.readFile("/etc/sp/sp.cfg");
    } catch (const libconfig::FileIOException &fioex) {
        throw ublox::config_error("I/O error while reading file.");
    } catch (const libconfig::ParseException &pex) {
        std::stringstream ss;
        ss << "Parse error at " << pex.getFile() << ":" << pex.getLine()
                  << " - " << pex.getError() << std::endl;
        throw ublox::config_error(ss.str().c_str());
    }

    const libconfig::Setting& root = cfg.getRoot();
    int port;
    if (!root["gps"].lookupValue("gps_port", port)) {
        throw ublox::config_error("GPS port not defined in config.");
    }

    if (!root["gps"].lookupValue("gps_dev", serial_dev)) {
        throw ublox::config_error("GPS device not defined in config.");
    }

    if (!root["gps"].lookupValue("gps_dev_baud", serial_baud)) {
        throw ublox::config_error("GPS baudrate not defined in config.");
    }
    spdlog::info("Config parsed.");

    try {
        context = zmq::context_t(1);
        socket = new zmq::socket_t(context, ZMQ_PUB);
        if (socket != nullptr) {
            socket->bind("tcp://*:" + std::to_string(port));
        } else {
            throw ublox::network_error("Error while creating socket.");
        }
    } catch (const zmq::error_t &e) {
        throw ublox::network_error(e.what());
    }
    spdlog::info("Socket opened at: {}", port);
    
    serial = serialOpen(serial_dev.c_str(), serial_baud);
    if (serial == -1) {
        socket->close();
        throw ublox::device_error("Error while opening serial device.");
    }
    spdlog::info("Serial connected to: {}", serial_dev);
    enabled = true;
}

ublox::GPS::~GPS() {
    socket->close();
    delete socket;
    context.close();
    serialClose(serial);
}

bool ublox::GPS::is_enabled() {
    return enabled;
}

bool ublox::GPS::is_healthy() {
    auto diff = std::chrono::system_clock().now() - last_device_transmission;
    return std::chrono::duration_cast<std::chrono::milliseconds>(diff) < std::chrono::milliseconds(5000);
}

std::vector<std::string> ublox::GPS::split(std::string str, char delim) {
    std::vector<std::string> result;
    std::stringstream ss(str);
    std::string item;
    while(std::getline(ss, item, delim)){
        result.push_back(item);
    }
    return result;
}

std::string ublox::GPS::read_data() {
    if (fcntl(serial, F_GETFD) == -1) { // serial connection check
        spdlog::error("Serial went down. Restarting...");
        serialClose(serial);
        int fd = serialOpen(serial_dev.c_str(), serial_baud);
        if (fd != -1) {
            spdlog::info("Serial connected.");
            serial = fd;
        } else {
            return std::string();
        }
    }

    int to_read = serialDataAvail(serial); 
    if (to_read == 0) {
        return std::string();
    }

    std::fill(serial_buffer, serial_buffer + serial_buffer_length, 0);
    read(serial, serial_buffer, to_read);
    return std::string(serial_buffer);
}

std::vector<gps_frame_t> ublox::GPS::parse_data(std::string data) {
    auto lines = split(data, '\n');
    std::vector<gps_frame_t> gps_frames;
    for (auto& l : lines){
        if (l.find("$GNGGA") != std::string::npos) {
            last_device_transmission = std::chrono::system_clock::now();
            auto tokens = split(l, ',');
            try {
                gps_frames.push_back(gps_frame_t(
                    std::stoi(tokens[nmea::time_pos]),
                    std::stod(tokens[nmea::lat_pos])/100.0,
                    tokens[nmea::lat_dir_pos][0],
                    std::stod(tokens[nmea::long_pos])/100.0,
                    tokens[nmea::long_dir_pos][0],
                    std::stoi(tokens[nmea::fix_quality_pos]),
                    std::stoi(tokens[nmea::satelites_pos]),
                    std::stod(tokens[nmea::alt_pos])
                ));
                spdlog::info("$GNGGA, {}, {}, {}, {}, {}, {}, {}, {}", 
                             gps_frames.back().time,
                             gps_frames.back().lat,
                             gps_frames.back().lat_dir,
                             gps_frames.back().lon,
                             gps_frames.back().lon_dir,
                             gps_frames.back().fix_quality,
                             gps_frames.back().satelites,
                             gps_frames.back().altitude
                             );
            } catch (const std::exception& e) {
                spdlog::error(e.what());
            }
        }
    }
    if (gps_frames.size() == 0) {
        throw ublox::parse_error("GNGGA not found.");
    } else {
        std::sort(gps_frames.begin(), gps_frames.end(), [](const gps_frame_t& lhs, const gps_frame_t& rhs){
            return lhs.time < rhs.time;
        });        
        return gps_frames;
    }
}

void ublox::GPS::send_data(std::vector<gps_frame_t>& gps_frames) {
    for(auto& frame : gps_frames) {
        std::string topic = "gps";
        zmq::message_t msg(&frame, sizeof(gps_frame_t));   
        try {
            socket->send(topic.begin(), topic.end(), ZMQ_SNDMORE);
            socket->send(msg);
        } catch (const zmq::error_t& err) {
            throw network_error(err.what());
        }
    }
}

void ublox::GPS::process() {
    if (is_enabled()) {
        auto data = read_data();
        if (data.length() > 0) {
            try { 
                auto frame = parse_data(data);
                send_data(frame);
            } catch (const parse_error& e){
                spdlog::error(e.what()); 
            }
        }
    }
}
